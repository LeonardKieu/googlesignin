import React from 'react'; import ReactDOM from 'react-dom';
import axios from 'axios';

class SignInComponent extends React.Component {
    constructor(props) {
        super(props);
        this.be_domain = "http://localhost:8000/graphql";
    }
    static getDerivedStateFromProps = (props, state) => { // 3 Rendering, pass favcol and return an obj. this.setState(obj)
        console.log(`Win localtion ${window.location.href}`);
        if (window.location.href.includes("consent")) {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            // myHeaders.append("Cookie", "csrftoken=MZ0PhOxkUZkP0SVGXw8dhJtDGlgIrn1gJqDzMRDajPZy8xVQ5XAoK5DHNOpYXbrS");
            console.log(`This is url ${window.location.href}`);
            var graphql = JSON.stringify({
                query: `
                    mutation{
                        claimTokenSignInGoogle(consent_url: "${window.location.href}"){
                        status
                        message
                        errors{
                            message
                            error_code
                        }
                        access_token
                        refresh_token
                        }
                    }
                `,
                variables: {}
            })
            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: graphql,
                redirect: 'follow'
            };

            fetch("http://localhost:8000/graphql", requestOptions)
                .then(response => response.text())
                .then(result => {
                    console.log(result);
                    let access_token = JSON.parse(result).data.claimTokenSignInGoogle.access_token;
                    let refresh_token = JSON.parse(result).data.claimTokenSignInGoogle.refresh_token;
                    console.log(`Access ${access_token}, refresh ${refresh_token}`);
                    let picture = JSON.parse(result).data.claimTokenSignInGoogle.picture;
                    console.log(`avatar ${picture}`);
                })
                .catch(error => console.log('error', error));
        }
        return { current_url: window.location.href };
    }

    render() {
        return <div>
            <h2>Sign in via Google here:</h2>
            <button id="signinButton" onClick={() => {
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                // myHeaders.append("Cookie", "csrftoken=MZ0PhOxkUZkP0SVGXw8dhJtDGlgIrn1gJqDzMRDajPZy8xVQ5XAoK5DHNOpYXbrS");

                var graphql = JSON.stringify({
                    query: "mutation{\n  redirectSignInViaGoogle{\n    status\n    message\n    errors{\n      message\n      error_code\n    }\n    redirect_url\n  }\n}",
                    variables: {}
                })
                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: graphql,
                    redirect: 'follow'
                };

                fetch(this.be_domain, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        console.log(result);
                        let redirect_url = JSON.parse(result).data.redirectSignInViaGoogle.redirect_url;
                        console.log(`Redirect url ${redirect_url}`);
                        window.location = redirect_url;
                    })
                    .catch(error => console.log('error', error));
            }}>React: Sign in Google</button>
            <img src={this.state.picture} />
        </div >;
    }
}

export default SignInComponent;